import React, { useState } from "react";
import TodoItems from "./TodoItems";
import AddList from "./addList";
import "./styles.css";
// import {Div} from "./styles"


function MyTodo() {
  const [newTodoName, setNewTodoName] = useState("");
  const [todoItemsArray, setTodoItemsArray] = useState([
    {
      name: "Wake up and Have Breakfast",
    },
    {
      name: "Take Out The Trash",
    },
    {
      name: "Get Ready Before 8am",
    },
    {
      name: "Go to The Grocery Shop After Work",
    },
    {
      name: "See The Dentist At 7pm",
    },
  ]);

  const handleDelete = (index) => {
    const newtodos = [...todoItemsArray];
    newtodos.splice(index, 1);
    setTodoItemsArray(newtodos);
  };

  // const handleDelete = (index) => {
  //   setTodoItemsArray(todoItemsArray.filter(TodoItems => todoItemsArray.index !== index))
  // };

  const handleChange = ({ target: { value } }) => {
    setNewTodoName(value)
  };

  const handleAddItems = () => {
    if (newTodoName !== "")
      setTodoItemsArray([{ name: newTodoName }, ...todoItemsArray]);
  };

  return (
    <div className="AppTodo">
      <AddList
        text="text"
        placeholder="Add a New Task"
        add="Add"
        handleSubmit={handleAddItems}
        handleChange={handleChange}
      />
      <nav className="todo-list">
        <TodoItems 
        todoItemsArray={todoItemsArray} 
        deleteItems={handleDelete} 
        />
      </nav>

    </div>
  );
}

export default MyTodo;
