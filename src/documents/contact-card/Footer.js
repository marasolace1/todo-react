import React from 'react';

function Footer() {
  //internal styling
  // const styles = {
  //   color: "red", 
  //   backgroundColor: "yellow",
  //   fontSize: 200 //or "200px"
  // }
  return (
    <div>
      <footer>
        <h3>This is my footer</h3>
        {/* <h3 style={styles}>This is my footer</h3> */}
        {// inline styling
        /* <h3 style={{color: "red", backgroundColor: "yellow"}}  >This is my footer</h3> */}
      </footer>
    </div>
  );
}

export default Footer;