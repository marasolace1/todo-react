import React from 'react';
import ContactCard from './contactCard'

function ContactDetails() {

  return (
    <div>
      <ContactCard 
        name="Mr. Whiskerson" 
        imgUrl="http://placekitten.com/300/200" 
        phone="(212) 555-1234" 
        email="mr.whiskaz@catnap.meow"
      />
      <ContactCard 
        name="Fluffykins" 
        imgUrl="http://placekitten.com/400/200" 
        phone="(212) 555-2345" 
        email="fluff@me.com"
      />
    </div>
  );
}

export default ContactDetails;