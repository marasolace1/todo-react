import React from 'react';

function Header() {
  const firstName = 'Solace'
  const lastName = 'Anyanwu'
  const date = new Date() //year, month, day, time 
  const hours = date.getHours()
  const styles = {
   FontSize: "50px"
  }

  let timeOfDay 
  if (hours < 12) {
    timeOfDay = 'morning'
    styles.color = 'green'
  } else if (hours >= 12 && hours <= 17){
    timeOfDay = 'afternoon'
    styles.color = 'purple'
  } else {
    timeOfDay = 'night'
    styles.color = 'orange'
  }

  return (
    <div>
      <h1 className='headerName'>Hello { `${firstName} ${lastName}`}</h1>
      {/* <h1 className='headerName'>Hello {firstName + " " + lastName} !</h1> */}
      <h3 style={styles}> Good{timeOfDay}!</h3>
      <p>A list of where i have been to</p>
      {/* <ul>
        <li>Nigeria</li>
          <ul>  
            <li>Abia</li>
            <li>Imo</li>
            <li>Ogun</li>
            <li>Enugu</li>
          </ul>
        <li>Canada</li>
          <ul>
            <li>Mc-Gill</li>
          </ul>
      </ul> */}
    </div>
  );
}

export default Header;