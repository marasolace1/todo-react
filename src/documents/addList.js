import React from "react";
// import {Div} from "./styles"

function AddList({text, placeholder, add, handleChange, handleSubmit }) {
  return (
    <div className="addTodo">
      <input type={text} placeholder={placeholder} onChange={handleChange} />
      <span onClick = {handleSubmit}>{add}</span>
    </div>
  );
}

export default AddList;
